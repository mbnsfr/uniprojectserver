const express = require("express");
const ParseServer = require("parse-server").ParseServer;
// import dotenv from "dotenv";

// dotenv.config();

const app = express();

// const PARSE_APP_ID = process.argv[2];
// const PARSE_MASTER_KEY = process.argv[3];
// const PARSE_PORT = process.argv[4];
// const PARSE_SERVER_DATABASE_URI = process.argv[5];
// const PARSE_SERVER_URL = process.argv[6];
// const PARSE_JAVASCRIPT_KEY = process.argv[7];
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const api = new ParseServer({
  databaseURI: "mongodb://127.0.0.1:27017/uniserver", // Connection string for your MongoDB database
  // cloud: "",
  appId: "uniservermyAppId",
  masterKey: "uniserverMasterKey", // Keep this key secret!
  fileKey: "optionalFileKey",
  serverURL: "http://localhost:1351/parse", // Don't forget to change to https if needed
  // serverURL: "https://localhost:1351/parse", // Don't forget to change to https if needed
  // javascriptKey: "",
  // restAPIKey: "",
});

// var api = new ParseServer({
//   databaseURI: databaseUri || 'mongodb://localhost:27017/dev',
//   cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
//   appId: process.env.APP_ID || 'myAppId',
//   restAPIKey: process.env.REST_API_KEY || '',
//   javascriptKey: process.env.JAVASCRIPT_KEY || '',
//   serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',
//   masterKey: process.env.MASTER_KEY || '' //Add your master key here. Keep it secret!
// });

// Serve the Parse API on the /parse URL prefix
app.use("/parse", api);

app.listen(1351, function() {
  console.log(`uni-server running on port 1351.`);
});
